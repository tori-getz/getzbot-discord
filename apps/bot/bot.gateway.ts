import { Once } from '@discord-nestjs/core';
import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class BotGateway {
    private readonly logger = new Logger(BotGateway.name)
    
    @Once('ready')
    public onStart () {
        this.logger.log('Bot started!');
    }
}
