import { ConfigService } from "@nestjs/config";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { FuckerEntity } from "../entities/fucker.entity";

export const getDatabaseConfig = (
    configService: ConfigService
): TypeOrmModuleOptions => {
    return {
        type: 'sqlite',
        database: configService.get<string>('DATABASE') ?? 'data.db',
        synchronize: true,
        logging: false, 
        entities: [
            FuckerEntity
        ]
    }
}
