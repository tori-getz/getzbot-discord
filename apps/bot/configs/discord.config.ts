import { DiscordModuleOption } from '@discord-nestjs/core';
import { ConfigService } from '@nestjs/config';
import { GatewayIntentBits } from 'discord.js';

export const getDiscordConfig = (
  configService: ConfigService,
): DiscordModuleOption => {
  return {
    token: configService.get<string>('DISCORD_BOT_TOKEN'),
    discordClientOptions: {
      intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages
      ],
    },
    registerCommandOptions: [
      {
        forGuild: configService.get<string>('GUILD_ID'),
        removeCommandsBefore: true
      }
    ],
  };
};
