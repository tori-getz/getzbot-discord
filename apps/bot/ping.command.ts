import { Command, DiscordCommand } from '@discord-nestjs/core';
import { Injectable } from '@nestjs/common';
// import { CommandInteraction } from 'discord.js';

@Command({
  name: 'ping',
  description: 'Просто пингует',
})
@Injectable()
export class PingCommand implements DiscordCommand {
  public handler (): string {

    return 'pong';
  }
}
