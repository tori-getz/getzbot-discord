import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FuckerEntity } from '../entities/fucker.entity';

@Injectable()
export class FuckService {
    public constructor (
        @InjectRepository(FuckerEntity) private readonly fuckerRepository: Repository<FuckerEntity>
    ) {}

    public async findAll (): Promise<FuckerEntity[]> {
        return await this.fuckerRepository.find();
    }

    public async findByName (name: string): Promise<FuckerEntity> {
        return await this.fuckerRepository.findOneBy({ name });
    }

    public async create (name: string): Promise<FuckerEntity> {
        return await this.fuckerRepository.save({
           name,
           count: 0 
        });
    }

    public async fuck (fucker: FuckerEntity): Promise<number> {
        fucker.count++;
        await this.fuckerRepository.save(fucker);
        return fucker.count;
    }
}
