const prefix = 'getzbot-';
module.exports = {
    apps: [
        {
            name: prefix + 'bot',
            script: 'yarn start'
        }
    ]
}
